
# Myco-Sagas

The SAGA standard is widely used and I consider it a standard in the micro services literature in contexts where we must maintain an ACID integrity in the ecosystem or simply if we need to centralize the rules of actions that depend on multiple agents to be fully executed.

The Myco-Sagas library aims to serve as an interface that facilitates the management of these two problems in the context of micro services through the use of the RabbitMQ queue manager.

More: 

https://dzone.com/articles/distributed-sagas-for-microservices

https://microservices.io/patterns/data/saga.html

# Setup
The file "sagas.json" serves to declare our Sagas as in a markup language.

### Attributes

##### name (string): 
Represents the name of the action that will be available externally on the requesting clients.

##### params (vector): 
Parameters declaration is possible as a form of pre-validation of attributes.Parameter declaration is optional. It is intended only to avoid overloading external services. Currently the accepted types are: 

 - "integer"
 - "float"
 - "string"
 - "boolean"

Example:

```sh
{
    "field":"user_id",
    "type": "integer", 
    "required": "True"
}
```

##### steps (vector): 
In this field we declare all the phases that make up our saga.
- main_function (required): function to execute
- rollback_function (optional): function to execute if execution of the saga fails
- critical (optional): if true and main_function fails, then the saga will fail. If it is false or not declared, even if the function main_function fails the saga will normally continue its flow.

Example:

```sh
{
    "main_function":"new_worker.error_demo_function", 
    "rollback_function":"new_worker.rollback_demo_function", 
    "critical":"True"
}
```

# Communication Architecture

Basically we have 3 actors in the context of running a saga.

### Client: 
Is the part that invokes a saga (ex: myco-api or myproject demand by the synchronization of a certain object). This part is responsible for through a rabbitmq interface, add a message in the ASYNC queue called "saga" with the description of the method (which refers to the SAGA name defined in sagas.json) and the message body to be propagated.

### SAGA Server: 
It is responsible for keeping alive the process that listens to the "saga" queue (saga_worker.py), validate the existence of the requested saga, as well as validate the request parameters. After validating the entries and loading the required SAGA, this actor is responsible for the control of the execution of the steps defined in "sagas.json" and also for the call of the rollbacks in case there are failures in the process. All running works in RPC with synchronous calls, waiting for the response of each step until you go to the next one.

### Service Server: 
Represents the end service to be run. As noted in the previous point, SAGA Server will make an RPC call in the queue of this service, so it is also necessary to have a process that will listen to this queue (example file in "external_worker_example.py)" and execute its function interpreting and returning if the action was well executed or not. In this execution point we can add a behavior to an internal service API call and validate whether or not it returns a 200 code, for example.
