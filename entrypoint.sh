#!/bin/bash
set -x

cp /usr/src/code/docker/local_settings.py notifications/settings/
cp /usr/src/code/docker/local_tests_settings.py notifications/settings/

python /usr/src/code/docker/wait_postgres.py

python manage.py migrate --noinput

[ -r /fixtures/fixtures.json ] && python manage.py loaddata /fixtures/fixtures.json

is_ready() {
    eval "curl -I http://${RABBIT_USER}:${RABBIT_PASSWORD}@${RABBIT_HOST}:${RABBIT_MANAGEMENT_PORT}/api/vhosts"
}

i=0
while ! is_ready; do
    i=`expr $i + 1`
    if [ $i -ge 10 ]; then
        echo "$(date) - rabbit still not ready, giving up"
        exit 1
    fi
    echo "$(date) - waiting for rabbit to be ready"
    sleep 3
done

python /usr/src/code/saga_worker.py &

sleep 36000

exec "$@"
