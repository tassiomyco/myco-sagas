import pika
import json
import requests
import os

my_path = os.path.abspath(os.path.dirname(__file__))
path = os.path.join(my_path, "settings.json")
with open(path, 'r') as f:
    config = json.load(f)

connection = pika.BlockingConnection(
    pika.ConnectionParameters(
        host=config['DEFAULT']['HOST'],
        port=config['DEFAULT']['PORT'],
        credentials=pika.PlainCredentials(
            username=config['DEFAULT']['USERNAME'],
            password=config['DEFAULT']['PASSWORD'])
    )
)

WORKER_NAME = 'new_worker'


channel = connection.channel()
channel.queue_declare(queue=WORKER_NAME, durable=True)

def success_demo_function(data):
    return {'data':'Demo rocks!'}

def rollback_demo_function(data):
    return {'data':'Rollback rocks!'}

def error_demo_function(data):
    raise Exception('API error!')

def send_response(ch, method, props, response):
    ch.basic_publish(exchange='',
                         routing_key=props.reply_to,
                         properties=pika.BasicProperties(
                             correlation_id=props.correlation_id),
                         body=json.dumps(response))
    ch.basic_ack(delivery_tag=method.delivery_tag)

functions = {
    'success_demo_function': success_demo_function,
    'error_demo_function': error_demo_function,
    'rollback_demo_function': rollback_demo_function
}

def validate_call(internal_method):
    return internal_method in functions

def load_body(body):
    body = body.decode("utf-8")
    body = json.loads(body)
    return (body['method'], body['params'])

def on_request(ch, method, props, body):
    response = {}
    response['jsonrpc'] = '2.0'
    internal_method, params = load_body(body)  

    if not validate_call(internal_method):
        response['error'] = 'Function does not exist.'
    else:
        try:
            response['result'] = functions[internal_method](params)        
        except Exception:
            response['error'] = 'Invalid params to internal request.'
    send_response(ch, method, props, response)   


channel.basic_consume(on_request, queue=WORKER_NAME)
print(" [x] Awaiting SYNC new_worker requests")
channel.start_consuming()
