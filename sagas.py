import requests
import json

BOOLEANS_FIELDS = ['True', 'False']

from myco_notifications.rpc import AsyncCall, SyncCall 
with open('settings.json', 'r') as f:
    config = json.load(f)

class Event:
    def __init__(self, worker, main_function, rollback_function, critical, next_event=None, previous_event=None):
        self.worker = worker        
        self.main_function = main_function
        self.rollback_function = rollback_function
        self.critical = critical
        self.next_event = next_event
        self.previous_event = previous_event


class Saga:
    def __init__(self, body=None, params=None):
        self.body = body
        self.params = params
        self.events = []
        self.head = None
        self.tail = None
        self.server_name = config['DEFAULT']['HOST']
        self.port = config['DEFAULT']['PORT']


    def add_event(self, worker, main_function, rollback_function, critical):
        event = Event(worker, main_function, rollback_function, critical)
        if not self.head:
            self.head = event
            self.tail = event
        else:
            event.previous_event = self.tail
            self.tail.next_event = event
            self.tail = event

    def apply_rollback(self, event):
        context = self.head

        while context is not event:
            if context.rollback_function:
                call = SyncCall(
                    queue=context.worker,
                    server_name=self.server_name,
                    port=self.port
                ).call(context.rollback_function, self.body)
                print('rollback - ',event.rollback_function,call)
            context = context.next_event

    def execute_saga(self, body):
        self.body = body
        context = self.head

        while context is not None:
            call = SyncCall(
                queue=context.worker,
                server_name=self.server_name,
                port=self.port
            ).call(context.main_function, self.body)
            print('main saga - ',call)
            if 'error' in call and context.critical:
                self.apply_rollback(context)
                break
            context = context.next_event

class SagaLoader():
    def __init__(self, filePath='sagas.json'):
        self.filePath = filePath
        self.file = []
        self.sagas = {}
        self.error = None
        self.load_file()
        self.validate_file()
        self.load_sagas()

    def validate_params(self, params):
        allow_types = ['integer', 'boolean', 'float', 'string']
        for param in params:
            if 'field' not in param:
                raise Exception("You should define a field.")
            if 'type' not in param:
                raise Exception("You should define a type to your field.")
            if param['type'] not in allow_types:
                raise Exception("Type of {} field is unsupported.".format(param['field']))
            if 'required' in param:
                if param['required'] not in BOOLEANS_FIELDS:
                    raise Exception("You have an incorrect value in your required field.")
        return True

    def validate_steps(self, steps):
        for step in steps:
            if 'main_function' not in step:
                raise Exception("You should define all main functions.")
            resource = step['main_function'].split('.')
            if len(resource) != 2:
                raise Exception("You have an incorrect format in your main_function!")
            if 'rollback_function' in step:
                resource = step['rollback_function'].split('.')
                if len(resource) != 2:
                    raise Exception("You have an incorrect format in your rollback_function!")
            if 'critical' in step:
                if step['critical'] not in BOOLEANS_FIELDS:
                    raise Exception("You have an incorrect value in your critical field.")
        return True

    def validate_headers(self, saga):
        if 'name' not in saga:
            raise Exception("You should define a name to your saga.")
        if 'steps' not in saga:
            raise Exception("You should define the steps to your saga.")
        if len(saga['steps']) == 0:
            raise Exception("You should define at last one step to your saga.")

    def validate_file(self):
        for saga in self.file:
            try:
                self.validate_headers(saga)
                # Params validation and description is optional
                if 'params' in saga:
                    self.validate_params(saga['params'])
                self.validate_steps(saga['steps'])
            except Exception as error:
                self.error = error

    def get_cast_function(self, type_obj):
        if type_obj == 'integer':
            return int
        elif type_obj == 'boolean':
            return bool
        elif type_obj == 'float':
            return float
        elif type_obj == 'string':
            return str

    def validate_body(self, params, body):
        for rule in params:
            try:
                func = self.get_cast_function(rule['type'])
                func(body[rule['field']])
            except Exception:
                raise Exception("{} is not a {}".format(rule['field'], rule['type']))
            if rule['required'] and rule['field'] not in body:
                raise Exception("{} is required.".format(rule['field']))
        return True

    def load_file(self):
        with open(self.filePath) as json_data:
            self.file = json.load(json_data)

    def load_sagas(self):
        if self.error:
            raise Exception(self.error)
        
        for saga in self.file:
            self.sagas[saga['name']] = self.create_saga(saga)

    def create_saga(self, saga_json):
        saga = Saga(params=saga_json['params'])
        for event in saga_json['steps']:
            worker, main_function = event['main_function'].split('.')
            if 'rollback_function' in event:
                _, rollback_function = event['rollback_function'].split('.')
            else:
                rollback_function = None
            critical = bool(event['critical'])
            saga.add_event(worker, main_function, rollback_function, critical)
        return saga


    def execute_saga(self, name, body):
        try:
            self.validate_body(self.sagas[name].params, body)
            self.sagas[name].execute_saga(body)
        except Exception as err:
            raise err
