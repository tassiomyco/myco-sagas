import pika
import json
import requests

from sagas import SagaLoader

with open('settings.json', 'r') as f:
    config = json.load(f)

connection = pika.BlockingConnection(
    pika.ConnectionParameters(
        host=config['DEFAULT']['HOST'],
        port=config['DEFAULT']['PORT'],
        credentials=pika.PlainCredentials(
            username=config['DEFAULT']['USERNAME'],
            password=config['DEFAULT']['PASSWORD'])
    )
)

channel = connection.channel()
channel.queue_declare(queue='saga', durable=True)

def load_body(body):
    body = body.decode("utf-8")
    body = json.loads(body)
    return (body['method'], body['params'])

def on_request(ch, method, props, body):
    saga, body = load_body(body)
    try:
        loader = SagaLoader()
        loader.execute_saga(saga, body)       
    except Exception as err:
        print("error: {}".format(err))
    ch.basic_ack(delivery_tag=method.delivery_tag)  


channel.basic_consume(on_request, queue='saga')
print(" [x] Awaiting ASYNC saga_worker requests")
channel.start_consuming()
