FROM python:3.5

ENV PYTHONUNBUFFERED 1

WORKDIR /code/

COPY . /code

RUN pip install -r requirements.pip --exists-action=w --src=/tmp

ENTRYPOINT ["/usr/src/code/entrypoint.sh"]


# Used to be consistent. These files would be overshadowed by docker-compose volumes